﻿using WeatherUWP.Errors;
using WeatherUWP.Model;
using WeatherUWP.Model.Wunderground;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WeatherUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            IWeatherProvider weather = new WundergroundProvider();
            WeatherCurrentConditionObj wInf = await weather.getWeatherCondition("Kiev");
            if (wInf != null)
            {                
                textBox.Text = wInf.tempratureString;
                listBox.Items.Add(wInf.weather);
                listBox.Items.Add("Date: " + wInf.periodTitle);
                listBox.Items.Add("Direction of the wind: " + wInf.windDir);
                listBox.Items.Add("Wind sped (kph): " + wInf.windKph);

                image.Source = new BitmapImage(new System.Uri(wInf.iconUrl, System.UriKind.Absolute));                
            }            
        }

        private void btnForecast_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ForecastPage));
        }
    }
}
