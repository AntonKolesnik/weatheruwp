﻿using WeatherUWP.Model;
using WeatherUWP.Model.Wunderground;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WeatherUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ForecastPage : Page
    {
        public ForecastPage()
        {
            this.InitializeComponent();
        }

        
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private async void btnBack_Copy_Click(object sender, RoutedEventArgs e)
        {
            WundergroundProvider weather = new WundergroundProvider();
            List<WeatherForecastObj> wInfList = await weather.getWeatherForecast("Kiev");

            if (wInfList != null)
            {
                foreach (WeatherForecastObj obj in wInfList)
                {
                    listBox.Items.Add(obj.periodTitle);
                    listBox.Items.Add(obj.weatherDescr);
                    listBox.Items.Add("");
                }
            }            
        }
    }
}
