﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherUWP.Model
{
    interface IWeatherProvider
    {
        Task<WeatherCurrentConditionObj> getWeatherCondition(string location);
        Task<List<WeatherForecastObj>> getWeatherForecast(string location);
    }
}
