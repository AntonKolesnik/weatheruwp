﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherUWP.Model
{
    public class WeatherCurrentConditionObj
    {
        public string periodTitle { get; set; }
        public string tempratureString { get; set; }
        public double tempC { get; set; }
        public double tempF { get; set; }
        public string weather { get; set; }
        public string windDir { get; set; }
        public double windKph { get; set; }
        public string iconUrl { get; set; }
    }
}
