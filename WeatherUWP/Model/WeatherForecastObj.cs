﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherUWP.Model
{
    class WeatherForecastObj
    {
        public string periodTitle { get; set; }
        public string iconUrl { get; set; }
        public string weatherDescr { get; set; }
    }
}
