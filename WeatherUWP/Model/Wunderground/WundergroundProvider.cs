﻿using WeatherUWP.Errors;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;


namespace WeatherUWP.Model.Wunderground
{
    class WundergroundProvider : IWeatherProvider
    {
        public async Task<WeatherCurrentConditionObj> getWeatherCondition(string location)
        {
            string uri = String.Format("http://api.wunderground.com/api/1ce4fdf3c30010d1/conditions/q/UR/{0}.json", location);

            HttpClient httpClient = new HttpClient();
            string jsonString = "";
            WeatherCurrentConditionObj wInf = null;
            HttpResponseMessage response = null;
            RootObjectCondition deserializedObject = null;

            try
            {
                response = await httpClient.GetAsync(uri);
                jsonString = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1] { '"' });
                deserializedObject = JsonConvert.DeserializeObject<RootObjectCondition>(jsonString);

                wInf = new WeatherCurrentConditionObj();

                wInf.periodTitle = deserializedObject.current_observation.observation_time_rfc822;
                wInf.tempratureString = deserializedObject.current_observation.temperature_string;
                wInf.tempC = deserializedObject.current_observation.temp_c;
                wInf.tempF = deserializedObject.current_observation.temp_f;
                wInf.weather = deserializedObject.current_observation.weather;
                wInf.windDir = deserializedObject.current_observation.wind_dir;
                wInf.windKph = deserializedObject.current_observation.wind_kph;
                wInf.iconUrl = deserializedObject.current_observation.icon_url;
            }
            catch (Exception exp)
            {
                ExceptionsEndPoint.Receive("Wunderground::getWeatherCondition()", exp);
                return null;
            }
                        

            

            return wInf;
        }

        public async Task<List<WeatherForecastObj>> getWeatherForecast(string location)
        {
            string uri = String.Format("http://api.wunderground.com/api/1ce4fdf3c30010d1/forecast/q/UR/{0}.json", location);

            HttpClient httpClient = new HttpClient();
            string jsonString = "";            
            HttpResponseMessage response = null;
            RootObjectForecast deserializedObject = null;
            List<WeatherForecastObj> result = new List<WeatherForecastObj>();

            try
            {
                response = await httpClient.GetAsync(uri);
                jsonString = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1] { '"' });
                deserializedObject = JsonConvert.DeserializeObject<RootObjectForecast>(jsonString);

                int countPeriods = deserializedObject.forecast.txt_forecast.forecastday.Count;
                for (int i = 0; i < countPeriods; i++)
                {
                    Forecastday forecDay = deserializedObject.forecast.txt_forecast.forecastday[i];

                    WeatherForecastObj wInf = new WeatherForecastObj();
                    wInf.periodTitle = forecDay.title;
                    wInf.iconUrl = forecDay.icon_url;
                    wInf.weatherDescr = forecDay.fcttext_metric;
                    result.Add(wInf);
                }
            }
            catch (Exception exp)
            {
                ExceptionsEndPoint.Receive("Wunderground::getWeatherForecast()", exp);
                return null;
            }

           
                   
            return result;
        }
    }
}
