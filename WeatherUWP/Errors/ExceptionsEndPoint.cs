﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace WeatherUWP.Errors
{
    class ExceptionsEndPoint
    {
        public static async void Receive(string destination, Exception exp)
        {
            var dialog = new Windows.UI.Popups.MessageDialog(exp.Message, destination);
            dialog.Commands.Add(new Windows.UI.Popups.UICommand("OK") { Id = 0 });
            dialog.DefaultCommandIndex = 0;
            var result = await dialog.ShowAsync();

        }
    }
}
